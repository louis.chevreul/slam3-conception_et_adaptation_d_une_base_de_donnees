import java.util.Scanner;


public class testjava {
	
	//declaration des variables
	public static int pourcentage, minutes, secondes, barres, refus, rang, total = 0;
	public static boolean abandon = false, discalif = false;
	public static String athlete;
	public static int resultats[] = new int[3];
	public static Scanner reader = new Scanner(System.in);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		affichePresentation();
		
		//si pas d'abandon
		if (!afficheEpreuve("Escrime")){
			//on lance l'epreuve d'escrime
			escrime();
			//on affiche les points
			affichePoints(0);
			
			//si l'athlete n'est pas discalifie
			if (!discalif){
				
				//si pas abandon
				if(!afficheEpreuve("Natation")){
					//on lance l'epreuve de natation
					natation();
					//on affiche les points
					affichePoints(1);
					
					//si pas discalifie
					if (!discalif){
						
						//si pas abandon
						if (!afficheEpreuve("Equitation")){
							//on lance l'epreuve d'equitation
							equitation();
							//on affiche les point
							affichePoints(2);
							
							if (!discalif){
								
								if (!afficheEpreuve("Course/Tir")){
									course();
									afficheFin();
								}
							}
						}
						
					}
				}
			}
		}
	}
	
	
	
	//fonction de presentation
	public static void affichePresentation(){
		System.out.println("Ce progrmme sert a suivre un athlete dans le deroulement des epreuves");
		System.out.println("Entrez vos nom et prenom :");
		//on recupere le nom/prenom de l'ahtlete
		athlete = reader.next();
	}
	
	public static void discalifie(){
		System.out.println("Vous etes discalifie");
	}
	
	public static void afficheFin(){
		System.out.println(athlete + " votre rang est : " + rang + ".\nVos points :");
		for (int i = 0; i < 3; i++){
			System.out.println((i+1) + " : " + resultats[i]);
		}
		System.out.println("Total : " + total);
	}
	
	//fonction qui affiche les points
	public static void affichePoints(int numEpreuve){
		//si l'athlete n'est pas discalifié on affiche ses points
		if (!discalif){
			total = 0;
			for (int i = 0; i<3; i++){
				total += resultats[i];
			}
			System.out.println("vous avez :" + resultats[numEpreuve] + " points.\nTotal :" + total);
		}
		else{
			discalifie();
		}
	}
	
	//fonction qui gere l'abandon avant une epreuve donnée
	public static boolean afficheEpreuve(String nomEpreuve){
		char rep;
		System.out.println("Epreuve : " + nomEpreuve + ".\nAvez vous abandonne(O/N) :");
		do{
			rep = reader.next().charAt(0);
		}while(rep != 'o' || rep != 'O' || rep != 'n' || rep != 'N');
		if (rep == 'O' || rep == 'O'){
			abandon = true;
		}
		
		return abandon;
	}
	
	//fonction qui gere l'epreuve d'escrime
	public static void escrime(){
			
		do{
			try{
				System.out.println("Entrez votre pourcentage :");
				pourcentage = reader.nextInt();
			}catch(Exception e){
				System.err.println("veuillez entrez un nombre");
				pourcentage = 101;
				String s = reader.next();
			}
		}while(pourcentage < 0 || pourcentage >100);
		
		if (pourcentage > 65){
			resultats[0] = 250;
		}
		else{
			if (pourcentage > 50){
				resultats[0] = 230;
			}
			else{
				if (pourcentage > 25){
					resultats[0] = 200;
				}
				else{
					if (pourcentage > 10){
						resultats[0] = 100;
					}
					else{
						discalif = true;
						//discalifie();
					}
				}
			}
		}
	}
	
	public static void natation(){
		
		do{
			try{
				System.out.println("Entrez vos minutes :");
				minutes = reader.nextInt();
			}catch(Exception e){
				System.err.println("veuillez entrez un nombre");
				minutes = -1;
				String s = reader.next();
			}
		}while(minutes < 0);
		
		
		do{
			System.out.println("Entrez vos secondes :");
			secondes = reader.nextInt();
		}while(secondes < 0);
		
		if (minutes >= 3 && secondes >= 30){
			discalif = true;
			//discalifie();
		}
		else{
			secondes += minutes * 60;
			resultats[1] = 250 + (150 - secondes);
		}
	}
	
	public static void equitation(){
		
		do{
			try{
				System.out.println("Entrez le nombre de barre tombee :");
				barres = reader.nextInt();
			}catch(Exception e){
				System.err.println("veuillez entrez un nombre");
				barres = -1;
				String s = reader.next();
			}
		}while(barres < 0);
		
		do{
			try{
				System.out.println("Entrez le nombre de refus :");
				refus = reader.nextInt();
			}catch(Exception e){
				System.err.println("veuillez entrez un nombre");
				refus = -1;
				String s = reader.next();
			}
		}while(refus < 0);
		
		if (refus >= 3){
			discalif = true;
			//discalifie();
		}
		else{
			if (10 * barres + 20 * refus >= 0 ){
				resultats[2] = 100 - (10 * barres + 20 * refus);
			}
			else{
				resultats[2] = 0;
			}
		}
	}
	
	public static void course(){
		do{
			try{
				System.out.println("Entrez votre rang :");
				rang = reader.nextInt();
			}catch(Exception e){
				System.err.println("veuillez entrez un rang(nombre)");
				rang = -1;
				String s = reader.next();
			}
		}while(rang < 0);
	}
}
